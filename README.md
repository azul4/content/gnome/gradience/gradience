# gradience

Change the look of Adwaita, with ease

https://github.com/GradienceTeam/Gradience

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/gnome/gradience/gradience.git
```

<br><br>

Required dependencies added to azul repository:

python-anyascii

python-cssutils

python-material-color-utilities

adw-gtk-theme (optional)

adw-gtk3


